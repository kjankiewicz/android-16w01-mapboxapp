package com.example.kjankiewicz.android_16w01_mapboxapp

/*
  Based on
  https://docs.mapbox.com/help/tutorials/android-navigation-sdk/
  Some corrections have been made

  To run this app - you have to get MAPBOX API ACCESS TOKEN
  https://www.mapbox.com/help/create-api-access-token/
  and set it into R.string.access_token

 */

import android.annotation.SuppressLint
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast

// classes needed to initialize map
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback

// classes needed to add the location component
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.mapboxsdk.location.LocationComponent
import com.mapbox.mapboxsdk.location.modes.CameraMode

// classes needed to add a marker
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage

// classes to calculate a route
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.DirectionsRoute
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.util.Log
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection

// classes needed to launch navigation UI
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), OnMapReadyCallback, MapboxMap.OnMapClickListener, PermissionsListener {
    // variables for adding location layer
    private lateinit var mapboxMap: MapboxMap
    // variables for adding location layer
    private lateinit var permissionsManager: PermissionsManager
    private lateinit var locationComponent: LocationComponent
    // variables for calculating and drawing a route
    private var currentRoute: DirectionsRoute? = null
    private var navigationMapRoute: NavigationMapRoute? = null

    // variables for calculating and drawing a route
    private var originPoint: Point? = null
    private var destinationPoint: Point? = null

    // variables for adding a marker
    private var symbolLayerIconFeatureList = ArrayList<Feature>()


    private fun setDestinationPoint(map: MapboxMap, point: LatLng) {
        destinationPoint = Point.fromLngLat(point.longitude, point.latitude)
        val source = map.style!!.getSourceAs<GeoJsonSource>("destination-source-id")
        symbolLayerIconFeatureList.add(Feature.fromGeometry(destinationPoint))
        source?.setGeoJson(FeatureCollection.fromFeatures(symbolLayerIconFeatureList))
    }

    private fun setOriginPoint(map: MapboxMap, point: LatLng) {
        originPoint = Point.fromLngLat(point.longitude, point.latitude)
        val source = map.style!!.getSourceAs<GeoJsonSource>("destination-source-id")
        symbolLayerIconFeatureList.add(Feature.fromGeometry(originPoint))
        source?.setGeoJson(FeatureCollection.fromFeatures(symbolLayerIconFeatureList))
    }

    private fun clearPositions(map: MapboxMap) {
        originPoint = null
        destinationPoint = null
        symbolLayerIconFeatureList.clear()
        val source = map.style!!.getSourceAs<GeoJsonSource>("destination-source-id")
        source?.setGeoJson(FeatureCollection.fromFeatures(symbolLayerIconFeatureList))

        with(startButton) {
            isEnabled = false
            setBackgroundResource(R.color.mapboxGrayLight)
        }
        navigationMapRoute?.let {
            it.updateRouteVisibilityTo(false)
            it.updateRouteArrowVisibilityTo(false)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mapbox.getInstance(this, getString(R.string.access_token))
        setContentView(R.layout.activity_main)
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)
    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        this.mapboxMap = mapboxMap
        mapboxMap.setStyle(getString(R.string.navigation_guidance_day)) { style ->
            enableLocationComponent(style)

            addSourceDestinationIconSymbolLayer(style)

            mapboxMap.addOnMapClickListener(this@MainActivity)

            startButton.setOnClickListener {
                val simulateRoute = true
                val options = NavigationLauncherOptions.builder()
                        .directionsRoute(currentRoute)
                        .shouldSimulateRoute(simulateRoute)
                        .build()
                // Call this method with Context from within an Activity
                NavigationLauncher.startNavigation(this@MainActivity, options)
            }
        }
    }

    private fun addSourceDestinationIconSymbolLayer(loadedMapStyle: Style) {
        loadedMapStyle.addImage("destination-icon-id",
                BitmapFactory.decodeResource(this.resources, R.drawable.mapbox_marker_icon_default))
        val geoJsonSource = GeoJsonSource("destination-source-id")
        loadedMapStyle.addSource(geoJsonSource)
        val destinationSymbolLayer = SymbolLayer("destination-symbol-layer-id", "destination-source-id")
        destinationSymbolLayer.withProperties(
                iconImage("destination-icon-id"),
                iconAllowOverlap(true),
                iconIgnorePlacement(true)
        )
        loadedMapStyle.addLayer(destinationSymbolLayer)
    }

    override fun onMapClick(point: LatLng): Boolean {
        if (originPoint != null) {
            if (destinationPoint != null) {
                clearPositions(mapboxMap)
            } else {
                setDestinationPoint(mapboxMap, point)
                getRoute(originPoint!!, destinationPoint!!)
            }
        } else {
            // origin position
            setOriginPoint(mapboxMap, point)
        }
        return true
    }

    @SuppressLint("LogNotTimber")
    private fun getRoute(origin: Point, destination: Point) {
        NavigationRoute.builder(this)
                .accessToken(Mapbox.getAccessToken()!!)
                .origin(origin)
                .destination(destination)
                .build()
                .getRoute(object : Callback<DirectionsResponse> {
                    override fun onResponse(call: Call<DirectionsResponse>,
                                            response: Response<DirectionsResponse>) {
                        // You can get the generic HTTP info about the response
                        Log.d(TAG, "Response code: " + response.code())
                        if (response.body() == null) {
                            Log.e(TAG, "No routes found, make sure you set the right user and access token.")
                            return
                        } else if (response.body()!!.routes().size < 1) {
                            Log.e(TAG, "No routes found")
                            return
                        }

                        currentRoute = response.body()!!.routes()[0]

                        // Draw the route on the map
                        with(navigationMapRoute) {
                            if (this != null) {
                                updateRouteVisibilityTo(false)
                                updateRouteArrowVisibilityTo(false)
                            } else {
                                navigationMapRoute = NavigationMapRoute(null,
                                        mapView, mapboxMap, R.style.NavigationMapRoute)
                            }
                        }
                        navigationMapRoute?.addRoute(currentRoute)
                        startButton.isEnabled = true
                        startButton.setBackgroundResource(R.color.mapboxBlue)
                    }

                    override fun onFailure(call: Call<DirectionsResponse>, throwable: Throwable) {
                        Log.e(TAG, "Error: " + throwable.message)
                    }
                })
    }

    @SuppressLint("MissingPermission")
    private fun enableLocationComponent(loadedMapStyle: Style) {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            // Activate the MapboxMap LocationComponent to show user location
            // Adding in LocationComponentOptions is also an optional parameter
            locationComponent = mapboxMap.locationComponent

            locationComponent.activateLocationComponent(
                    LocationComponentActivationOptions.builder(this, loadedMapStyle).build())

            locationComponent.isLocationComponentEnabled = true
            // Set the component's camera mode
            locationComponent.cameraMode = CameraMode.TRACKING
        } else {
            permissionsManager = PermissionsManager(this)
            permissionsManager.requestLocationPermissions(this)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onExplanationNeeded(permissionsToExplain: List<String>) {
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show()
    }

    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            enableLocationComponent(mapboxMap.style!!)
        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show()
            finish()
        }
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    companion object {
        private const val TAG = "MapBoxApp-MainActivity"
    }
}
