Previous version was based on:
  https://www.mapbox.com/help/android-navigation-sdk/

Some corrections have been made

To run this app - you have to get MAPBOX API ACCESS TOKEN
  https://www.mapbox.com/help/create-api-access-token/
  and set it into R.string.access_token